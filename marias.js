var glCurrResultTotals = [0,0,0,0];
var glCurrGroupResultTotals = [0,0,0,0];
var glCurrGroupPointsResultTotals = [0,0,0,0];
var glCurrGroupCntAttackResultTotals = [0,0,0,0];
var glCurrGroupCntDefenseResultTotals = [0,0,0,0];
var glCurrGameId;
var glCurrSessionId;
var glCurrSessionMaxId;
var glCurrSessionList = "";
var glCurrNext = -1;

var names = [];
var widthCoef;

//Called when application is started.
function OnStart() {

    app.SetOrientation("Portrait", null);
    
    //Create main layout vertical
    layMain = app.CreateLayout( "linear", "Vertical,FillXY" );    
    
    //Create a layout with objects vertically centered.
    layGames = app.CreateLayout( "linear", "Horizontal" );    
    layMain.AddChild( layGames );

    //Create spinner game select.
    spinGames = app.CreateSpinner( "", 0.3 );
    spinGames.SetOnTouch( spinGames_OnChange );
    layGames.AddChild( spinGames );
    
    btnNewGame = app.CreateButton( "new group",  0.3, -1 );
    btnNewGame.SetOnTouch( btnNewGame_OnTouch );
    layGames.AddChild( btnNewGame );    

    btnDeleteGame = app.CreateButton( "delete group",  0.3, -1 );
    btnDeleteGame.SetOnTouch( btnDeleteGame_OnTouch );
    layGames.AddChild( btnDeleteGame );    
    
    //Create a layout with objects vertically centered.
    laySession = app.CreateLayout( "linear", "Horizontal" );    
    layMain.AddChild( laySession );

    //Create spinner session select.
    spinSessions = app.CreateSpinner( "", 0.225 );
    spinSessions.SetOnTouch( spinSessions_OnChange );
    laySession.AddChild( spinSessions );
    
    btnNewSession = app.CreateButton( "new s.",  0.225, -1 );
    btnNewSession.SetOnTouch( btnNewSession_OnTouch );
    laySession.AddChild( btnNewSession );    

    btnDeleteSession = app.CreateButton( "delete s.",  0.225, -1 );
    btnDeleteSession.SetOnTouch( btnDeleteSession_OnTouch );
    laySession.AddChild( btnDeleteSession );    

    btnDeleteLastGame = app.CreateButton( "del. l. g.",  0.225, -1 );
    btnDeleteLastGame.SetOnTouch( btnDeleteLastGame_OnTouch );
    laySession.AddChild( btnDeleteLastGame );    
    
    // --- end of navigation buttons
    //Add layout to app.    
    app.AddLayout( layMain );
    
    // initialize db
    initDb();
    
    refreshSpinGames();
    
}

function initDb() {
    
    //Create or open a database called "MyData".  
    db = app.OpenDatabase( "marias_test" );  
    //db = app.OpenDatabase( "marias" );  
    
    //db.Delete();
    //db.ExecuteSql( "DROP TABLE plays" );
    //db.ExecuteSql( "DROP TABLE games" );
    
    //Create a table (if it does not exist already).  
    db.ExecuteSql( "CREATE TABLE IF NOT EXISTS games " +  
        "(id integer primary key, player1 text, player2 text, player3 text, player4 text)" );  
        
    db.ExecuteSql( "CREATE TABLE IF NOT EXISTS plays " +  
        "(id integer primary key, " + 
        "game_id integer, " + 
        "session integer, " + 
        "player1_amount numeric, player2_amount numeric, player3_amount numeric, player4_amount numeric, " +
        "FOREIGN KEY(game_id) REFERENCES games(id) )" );     
        
    // upgrade to 4 player games
    db.ExecuteSql( "ALTER TABLE games ADD COLUMN player4 text" );     
    db.ExecuteSql( "ALTER TABLE plays ADD COLUMN player4_amount numeric" );     
    // setting player4_amount to 0 if null due to upgrade to 4 player game type
    db.ExecuteSql( "UPDATE plays SET player4_amount = 0 WHERE player4_amount IS NULL" );     
        
}

function setNextPlayer(){
    
    glCurrNext++;  
    
    if ( glCurrNext >= names.length ){
        glCurrNext = 0;
    }
    
    
    for ( j=0; j<names.length; j++){
        txtPlayerTitle[ names[j] ].SetTextColor("white");
    }
    
    txtPlayerTitle[ names[glCurrNext] ].SetTextColor("yellow");
    
}

function clearLayer(){
    
    if ( typeof layPNames !== 'undefined' ){
        
        layPNames.Hide();
        layPResults.Hide();
        btnNewResult.Hide();
        scrollPlays.Hide();
        layResults.Hide();
        layGroupResults.Hide();
        layGroupPointsResults.Hide();
        layGroupCntAttackResults.Hide();
        layGroupCntDefenseResults.Hide();
        txtFooter.Hide();
        
        layMain.DestroyChild(layPNames);
        layMain.DestroyChild(layPResults);
        layMain.DestroyChild(btnNewResult);
        layMain.DestroyChild(scrollPlays);
        layMain.DestroyChild(layResults);
        layMain.DestroyChild(layGroupResults);
        layMain.DestroyChild(layGroupPointsResults);
        layMain.DestroyChild(layGroupCntAttackResults);
        layMain.DestroyChild(layGroupCntDefenseResults);
        layMain.DestroyChild(txtFooter);
        
    }
}


function btnNewGame_OnTouch(){
    
    //Create dialog window.
    dlgTxt = app.CreateDialog( "enter player names" );
    
    //Create a layout for dialog.
    layDlg = app.CreateLayout( "linear", "vertical,fillxy" );
    layDlg.SetPadding( 0.02, 0, 0.02, 0.02 );
    dlgTxt.AddLayout( layDlg );

    //Create a list control.
    edtPlayer1ng = app.CreateTextEdit( "", 0.6, -1 );
    edtPlayer1ng.SetHint( "player 1" );
    layDlg.AddChild( edtPlayer1ng );    
    
    edtPlayer2ng = app.CreateTextEdit( "", 0.6, -1 );
    edtPlayer2ng.SetHint( "player 2" );
    layDlg.AddChild( edtPlayer2ng );    

    edtPlayer3ng = app.CreateTextEdit( "", 0.6, -1 );
    edtPlayer3ng.SetHint( "player 3" );
    layDlg.AddChild( edtPlayer3ng );    

    edtPlayer4ng = app.CreateTextEdit( "", 0.6, -1 );
    edtPlayer4ng.SetHint( "player 4 (optional)" );
    layDlg.AddChild( edtPlayer4ng );    

    //Create a layout with objects vertically centered.
    layConfirm = app.CreateLayout( "linear", "Horizontal" );    
    layDlg.AddChild( layConfirm );

    btnOkNames = app.CreateButton( "ok",  0.3, -1 );
    btnOkNames.SetOnTouch( btnOkNames_OnTouch );
    layConfirm.AddChild( btnOkNames );    

    btnCancelNames = app.CreateButton( "cancel",  0.3, -1 );
    btnCancelNames.SetOnTouch( btnCancelNames_OnTouch );
    layConfirm.AddChild( btnCancelNames );    

    //Show dialog.
    dlgTxt.Show();
    
}

function btnDeleteGame_OnTouch() {


    //Create dialog window.
    dlgDelG = app.CreateDialog( "confirm delete" );
    
    //Create a layout for dialog.
    layDlg = app.CreateLayout( "linear", "vertical,fillxy" );
    layDlg.SetPadding( 0.02, 0, 0.02, 0.02 );
    dlgDelG.AddLayout( layDlg );


    txtDeleteGroup = app.CreateText( "", 0.9, -1 );
    txtDeleteGroup.SetMargins( 0, 0.02, 0, 0.02 );
    txtDeleteGroup.SetText( "delete of current group and all its sessions!?" );
    layDlg.AddChild( txtDeleteGroup );    

    //Create a layout with objects vertically centered.
    layConfirm = app.CreateLayout( "linear", "Horizontal" );    
    layDlg.AddChild( layConfirm );

    btnOkDelGroup = app.CreateButton( "ok",  0.3, -1 );
    btnOkDelGroup.SetOnTouch( btnOkDeleteGroup_OnTouch );
    layConfirm.AddChild( btnOkDelGroup );    

    btnCancelDelGroup = app.CreateButton( "cancel",  0.3, -1 );
    btnCancelDelGroup.SetOnTouch( btnCancelDeleteGroup_OnTouch );
    layConfirm.AddChild( btnCancelDelGroup );

    //Show dialog.
    dlgDelG.Show();

}


function btnOkDeleteGroup_OnTouch(){

    dlgDelG.Hide();

    if ( spinGames.GetText() ){
        
        idsToDelete = spinGames.GetText().split(":");
        glCurrGameId = parseInt(idsToDelete[0]);
        
        console.log("id to delete: " + glCurrGameId );

        clearLayer();    
        
        db.ExecuteSql( "DELETE FROM games WHERE id = " + glCurrGameId, [], refreshSpinGames );   
        
        // should be done by casacde delete
        db.ExecuteSql( "DELETE FROM plays WHERE game_id = " + glCurrGameId  ); 
    }
}



function btnCancelDeleteGroup_OnTouch() {
    
    dlgDelG.Hide();
    
}


function btnOkNames_OnTouch() {
    
    if ( !edtPlayer1ng.GetText() || !edtPlayer2ng.GetText() || !edtPlayer3ng.GetText() ){
        app.ShowPopup( "some of the names is not defined" );
        return;
    }
    
    db.ExecuteSql( "INSERT INTO games (player1, player2, player3, player4)" +   
        " VALUES (?,?,?,?)", [edtPlayer1ng.GetText(), edtPlayer2ng.GetText(),edtPlayer3ng.GetText(),edtPlayer4ng.GetText()], 
        OnNamesOk, 
        OnNamesError );  
}


function btnNewSession_OnTouch() {


    if ( glCurrGameId ){
        console.log("glCurrGameId: " + glCurrGameId );
        if ( glCurrSessionId == 0 ){
            glCurrSessionMaxId = 1;
            glCurrSessionList = "1";
            glCurrSessionId = 1;
        } else {
            glCurrSessionMaxId = glCurrSessionMaxId + 1;
            glCurrSessionList = glCurrSessionList + "," + glCurrSessionMaxId;
            glCurrSessionId = glCurrSessionMaxId;
        }
        
        spinSessions.SetList( glCurrSessionList );
        
        spinSessions.SelectItem( glCurrSessionId );
        
        refreshResults();
        
        console.log( "add session: " + glCurrSessionId );
        
    }
}


function btnDeleteSession_OnTouch() {


    //Create dialog window.
    dlgDelS = app.CreateDialog( "confirm delete" );
    
    //Create a layout for dialog.
    layDlg = app.CreateLayout( "linear", "vertical,fillxy" );
    layDlg.SetPadding( 0.02, 0, 0.02, 0.02 );
    dlgDelS.AddLayout( layDlg );


    txtDeleteSession = app.CreateText( "", 0.9, -1 );
    txtDeleteSession.SetMargins( 0, 0.02, 0, 0.02 );
    txtDeleteSession.SetText( "delete of current session?" );
    layDlg.AddChild( txtDeleteSession );


    //Create a layout with objects vertically centered.
    layConfirm = app.CreateLayout( "linear", "Horizontal" );    
    layDlg.AddChild( layConfirm );

    btnOkDelSession = app.CreateButton( "ok",  0.3, -1 );
    btnOkDelSession.SetOnTouch( btnOkDeleteSession_OnTouch );
    layConfirm.AddChild( btnOkDelSession );    

    btnCancelDelSession = app.CreateButton( "cancel",  0.3, -1 );
    btnCancelDelSession.SetOnTouch( btnCancelDeleteSession_OnTouch );
    layConfirm.AddChild( btnCancelDelSession );    

    //Show dialog.
    dlgDelS.Show();

}


function btnOkDeleteSession_OnTouch(){

    dlgDelS.Hide();

    if ( spinGames.GetText() ){
        
        idsToDelete = spinGames.GetText().split(":");
        glCurrGameId = parseInt(idsToDelete[0]);
        
        console.log("id-session to delete: " + glCurrGameId + "-" + glCurrSessionId );
        db.ExecuteSql( "DELETE FROM plays WHERE game_id = " + glCurrGameId + " AND session = " + glCurrSessionId, [], refreshSessions );   

    }
}


function btnCancelDeleteSession_OnTouch(){
    
    dlgDelS.Hide();
    
}


function btnDeleteLastGame_OnTouch(){
    
    
    //Create dialog window.
    dlgDelLG = app.CreateDialog( "confirm delete" );
    
    //Create a layout for dialog.
    layDlg = app.CreateLayout( "linear", "vertical,fillxy" );
    layDlg.SetPadding( 0.02, 0, 0.02, 0.02 );
    dlgDelLG.AddLayout( layDlg );


    txtDeleteLastGame = app.CreateText( "", 0.9, -1 );
    txtDeleteLastGame.SetMargins( 0, 0.02, 0, 0.02 );
    txtDeleteLastGame.SetText( "delete of last game in current session?" );
    layDlg.AddChild( txtDeleteLastGame );    


    //Create a layout with objects vertically centered.
    layConfirm = app.CreateLayout( "linear", "Horizontal" );    
    layDlg.AddChild( layConfirm );

    btnOkDelLastGame = app.CreateButton( "ok",  0.3, -1 );
    btnOkDelLastGame.SetOnTouch( btnOkDeleteLastGame_OnTouch );
    layConfirm.AddChild( btnOkDelLastGame );    

    btnCancelDelLastGame = app.CreateButton( "cancel",  0.3, -1 );
    btnCancelDelLastGame.SetOnTouch( btnCancelDeleteLastGame_OnTouch );
    layConfirm.AddChild( btnCancelDelLastGame );    

    //Show dialog.
    dlgDelLG.Show();
    
}


function btnOkDeleteLastGame_OnTouch(){

    dlgDelLG.Hide();
    db.ExecuteSql( "DELETE FROM plays WHERE id IN (SELECT max(id) FROM plays WHERE game_id = " + glCurrGameId + " AND session = " + glCurrSessionId + ")", [], refreshSessions );   
}


function btnCancelDeleteLastGame_OnTouch(){

    dlgDelLG.Hide();
}


function refreshSpinGames(){

    db.ExecuteSql( "SELECT * FROM games ORDER BY id;", [], OnGamesResult ); 
}


function refreshSessions() {
 
    db.ExecuteSql( "SELECT DISTINCT session FROM plays WHERE game_id = ? ORDER BY 1;", [glCurrGameId], OnSessionResult );
}


function refreshResults() {
    
    db.ExecuteSql( "SELECT id, player1_amount, player2_amount, player3_amount, player4_amount FROM plays WHERE game_id = ? AND session = ? ORDER BY 1;", [glCurrGameId, glCurrSessionId], OnResultsResult );
}


function OnGamesResult(results){

    var s = "";  
    var tmp = "";
    var len = results.rows.length;  
    for(var i = 0; i < len; i++ ) {  
        var item = results.rows.item(i)
        
        if ( s ){
            s += ","
        }
        if ( item.player4 ){
            tmp = item.id + ":" + item.player1 + "-" + item.player2 + "-" + item.player3 + "-" + item.player4;
        } else {
            tmp = item.id + ":" + item.player1 + "-" + item.player2 + "-" + item.player3;
        }
        glCurrGameId = parseInt(item.id);    
        s += tmp;   
    }  
    
    spinGames.SetList( s );
    
    spinGames.SelectItem( tmp );
    
    setNames(tmp);
    
    refreshSessions();
}


function OnSessionResult( results ){

    glCurrSessionId = 0;
    glCurrSessionMaxId = 0;
    glCurrSessionList = "";

    var s = "";  
    var tmp = "";
    var len = results.rows.length;  
    for(var i = 0; i < len; i++ ) {  
        
        console.log("in session loop");
        var item = results.rows.item(i)
        
        if ( s.length > 0 ){
            s += ","
        } 
        tmp = item.session;
        glCurrSessionId = parseInt(tmp);   
        glCurrSessionMaxId = glCurrSessionId;   
        s += tmp;   
    }  
    
    glCurrSessionList = s;
    
    spinSessions.SetList( s );
    spinSessions.SelectItem( tmp );  
    
    refreshResults();
    
    refreshStats();
    
}



function OnGroupResultsResult( results ){


    var len = results.rows.length;  

    glCurrGroupResultTotals = [0,0,0,0];

    for(var i = 0; i < len; i++ ) {  
        var item = results.rows.item(i)
        
        var pr1;
        if ( item.sp1 ){
            pr1 = item.sp1.toFixed(1);
            glCurrGroupResultTotals[0] = glCurrGroupResultTotals[0] + item.sp1;
        } else {
            pr1 = "-";
        }

        var pr2;
        if ( item.sp2 ){
            pr2 = item.sp2.toFixed(1);
            glCurrGroupResultTotals[1] = glCurrGroupResultTotals[1] + item.sp2;
        } else {
            pr2 = "-";
        }
        var pr3;
        if ( item.sp3 ){
            pr3 = item.sp3.toFixed(1);
            glCurrGroupResultTotals[2] = glCurrGroupResultTotals[2] + item.sp3;
        } else {
            pr3 = "-";
        }
        var pr4;
        if ( item.sp4 ){
            pr4 = item.sp4.toFixed(1);
            glCurrGroupResultTotals[3] = glCurrGroupResultTotals[3] + item.sp4;
        } else {
            pr4 = "-";
        }
    }    
    
    //update group totals
    for ( i=0; i<names.length; i++ ){
        if (glCurrGroupResultTotals[i] < 0 ) {
            txtGroupPlayerResults[ names[i] ].SetTextColor("red");
        } else if ( glCurrGroupResultTotals[i] > 0 ){
            txtGroupPlayerResults[ names[i] ].SetTextColor("green");
        } else {
            txtGroupPlayerResults[ names[i] ].SetTextColor("white");
        }
        txtGroupPlayerResults[ names[i] ].SetText( glCurrGroupResultTotals[i].toFixed(1) );
    }
    
    
}


function getColor( values, value ){

    range = names.length;

    if( range == 3 ){
        maxValue = Math.max( values[0], values[1], values[2] );
        minValue = Math.min( values[0], values[1], values[2] );
    } else {
        maxValue = Math.max( values[0], values[1], values[2], values[3] );
        minValue = Math.min( values[0], values[1], values[2], values[3] );
    }

    if ( value == maxValue ){
        return "#00ace6";    
    } else if ( value == minValue ) {
        return "#ccf2ff";
    } else {
        return "#66d9ff";   
    }
}


function OnGroupPointsResultsResult( results ){


    var len = results.rows.length;  

    glCurrGroupPointsResultTotals = [0,0,0,0];

    for(var i = 0; i < len; i++ ) {  
        var item = results.rows.item(i)
        
        var pr1;
        if ( item.point1 ){
            pr1 = item.point1.toFixed(1);
            glCurrGroupPointsResultTotals[0] = glCurrGroupPointsResultTotals[0] + item.point1;
        } else {
            pr1 = "-";
        }

        var pr2;
        if ( item.point2 ){
            pr2 = item.point2.toFixed(1);
            glCurrGroupPointsResultTotals[1] = glCurrGroupPointsResultTotals[1] + item.point2;
        } else {
            pr2 = "-";
        }
        var pr3;
        if ( item.point3 ){
            pr3 = item.point3.toFixed(1);
            glCurrGroupPointsResultTotals[2] = glCurrGroupPointsResultTotals[2] + item.point3;
        } else {
            pr3 = "-";
        }
        var pr4;
        if ( item.point4 ){
            pr4 = item.point4.toFixed(1);
            glCurrGroupPointsResultTotals[3] = glCurrGroupPointsResultTotals[3] + item.point4;
        } else {
            pr4 = "-";
        }
    }    
    
    //update group totals
    for ( i=0; i<names.length; i++ ){
        txtGroupPointsPlayerResults[ names[i] ].SetTextColor(getColor( glCurrGroupPointsResultTotals, glCurrGroupPointsResultTotals[i] ));
        txtGroupPointsPlayerResults[ names[i] ].SetText( glCurrGroupPointsResultTotals[i].toFixed(1) );
    }
}


function OnGroupCntAttackResultsResult( results ){
    
    var len = results.rows.length;  

    glCurrGroupCntAttackResultTotals = [0,0,0,0];

    for(var i = 0; i < len; i++ ) {  
        var item = results.rows.item(i)
        
        var pr1;
        if ( item.cntatt1 ){
            pr1 = item.cntatt1.toFixed(0);
            glCurrGroupCntAttackResultTotals[0] = glCurrGroupCntAttackResultTotals[0] + item.cntatt1;
        } else {
            pr1 = "-";
        }

        var pr2;
        if ( item.cntatt2 ){
            pr2 = item.cntatt2.toFixed(0);
            glCurrGroupCntAttackResultTotals[1] = glCurrGroupCntAttackResultTotals[1] + item.cntatt2;
        } else {
            pr2 = "-";
        }
        var pr3;
        if ( item.cntatt3 ){
            pr3 = item.cntatt3.toFixed(0);
            glCurrGroupCntAttackResultTotals[2] = glCurrGroupCntAttackResultTotals[2] + item.cntatt3;
        } else {
            pr3 = "-";
        }
        var pr4;
        if ( item.cntatt4 ){
            pr4 = item.cntatt4.toFixed(0);
            glCurrGroupCntAttackResultTotals[3] = glCurrGroupCntAttackResultTotals[3] + item.cntatt4;
        } else {
            pr4 = "-";
        }
    }    
    
    //update group totals
    for ( i=0; i<names.length; i++ ){
        txtGroupCntAttackPlayerResults[ names[i] ].SetTextColor(getColor( glCurrGroupCntAttackResultTotals, glCurrGroupCntAttackResultTotals[i] ));
        txtGroupCntAttackPlayerResults[ names[i] ].SetText( glCurrGroupCntAttackResultTotals[i].toFixed(0) );
    }
}


function OnGroupCntDefenseResultsResult( results ){
    
    var len = results.rows.length;  

    glCurrGroupCntDefenseResultTotals = [0,0,0,0];

    for(var i = 0; i < len; i++ ) {  
        var item = results.rows.item(i)
        
        var pr1;
        if ( item.cntdef1 ){
            pr1 = item.cntdef1.toFixed(0);
            glCurrGroupCntDefenseResultTotals[0] = glCurrGroupCntDefenseResultTotals[0] + item.cntdef1;
        } else {
            pr1 = "-";
        }

        var pr2;
        if ( item.cntdef2 ){
            pr2 = item.cntdef2.toFixed(0);
            glCurrGroupCntDefenseResultTotals[1] = glCurrGroupCntDefenseResultTotals[1] + item.cntdef2;
        } else {
            pr2 = "-";
        }
        var pr3;
        if ( item.cntdef3 ){
            pr3 = item.cntdef3.toFixed(0);
            glCurrGroupCntDefenseResultTotals[2] = glCurrGroupCntDefenseResultTotals[2] + item.cntdef3;
        } else {
            pr3 = "-";
        }
        var pr4;
        if ( item.cntdef4 ){
            pr4 = item.cntdef4.toFixed(0);
            glCurrGroupCntDefenseResultTotals[3] = glCurrGroupCntDefenseResultTotals[3] + item.cntdef4;
        } else {
            pr4 = "-";
        }
    }    
    
    //update group totals
    for ( i=0; i<names.length; i++ ){
        txtGroupCntDefensePlayerResults[ names[i] ].SetTextColor(getColor( glCurrGroupCntDefenseResultTotals, glCurrGroupCntDefenseResultTotals[i] ));        
        txtGroupCntDefensePlayerResults[ names[i] ].SetText( glCurrGroupCntDefenseResultTotals[i].toFixed(0) );
    }
}

function refreshStats(){
    
    // refresh group totals    
    console.log("glCurrGameID: " + glCurrGameId );
    db.ExecuteSql( "SELECT sum(player1_amount) as sp1, sum(player2_amount) as sp2, sum(player3_amount) as sp3, sum(player4_amount) as sp4 FROM plays WHERE game_id = ? ORDER BY 1;", [glCurrGameId], OnGroupResultsResult );

    db.ExecuteSql( "SELECT SUM(CASE WHEN player1_amount>0 THEN CASE WHEN player1_amount = max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0.5 END END) as point1, " +
                    "  SUM(CASE WHEN player2_amount>0 THEN CASE WHEN player2_amount = max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0.5 END END) as point2, " +
                    "  SUM(CASE WHEN player3_amount>0 THEN CASE WHEN player3_amount = max(abs(player2_amount),abs(player1_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0.5 END END) as point3, " +
                    "  SUM(CASE WHEN player4_amount>0 THEN CASE WHEN player4_amount = max(abs(player1_amount),abs(player3_amount),abs(player4_amount),abs(player2_amount)) THEN 1 ELSE 0.5 END END) as point4 FROM plays WHERE game_id = ? ORDER BY 1;", [glCurrGameId], OnGroupPointsResultsResult );

    // cnt attack
    db.ExecuteSql( "SELECT SUM(CASE WHEN player1_amount>0 THEN CASE WHEN player1_amount = max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntatt1, " +
                    "  SUM(CASE WHEN player2_amount>0 THEN CASE WHEN player2_amount = max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntatt2, " +
                    "  SUM(CASE WHEN player3_amount>0 THEN CASE WHEN player3_amount = max(abs(player2_amount),abs(player1_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntatt3, " +
                    "  SUM(CASE WHEN player4_amount>0 THEN CASE WHEN player4_amount = max(abs(player1_amount),abs(player3_amount),abs(player4_amount),abs(player2_amount)) THEN 1 ELSE 0 END END) as cntatt4 FROM plays WHERE game_id = ? ORDER BY 1;", [glCurrGameId], OnGroupCntAttackResultsResult );

    // cnt defense
    db.ExecuteSql( "SELECT SUM(CASE WHEN player1_amount>0 THEN CASE WHEN player1_amount < max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntdef1, " +
                    "  SUM(CASE WHEN player2_amount>0 THEN CASE WHEN player2_amount < max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntdef2, " +
                    "  SUM(CASE WHEN player3_amount>0 THEN CASE WHEN player3_amount < max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntdef3, " +
                    "  SUM(CASE WHEN player4_amount>0 THEN CASE WHEN player4_amount < max(abs(player1_amount),abs(player2_amount),abs(player3_amount),abs(player4_amount)) THEN 1 ELSE 0 END END) as cntdef4 FROM plays WHERE game_id = ? ORDER BY 1;", [glCurrGameId], OnGroupCntDefenseResultsResult );

}


function OnResultsResult( results ){

    scrollPlays.RemoveChild(layScroll);
       
    layScroll = app.CreateLayout( "Linear", "Vertical,FillX" );
    scrollPlays.AddChild( layScroll );
    
    
    for (i=0;i<names.length; i++){
        glCurrResultTotals[i] = 0;
        
        if (glCurrResultTotals[i] < 0 ) {
            txtPlayerResults[names[i]].SetTextColor("red");
        } else if ( glCurrResultTotals[i] > 0 ){
            txtPlayerResults[names[i]].SetTextColor("green");
        } else {
            txtPlayerResults[names[i]].SetTextColor("white");
        }
        txtPlayerResults[names[i]].SetText( glCurrResultTotals[0].toFixed(1) );
        
    }
    
    var s = "";  
    var tmp = "";
    var len = results.rows.length;  
    
    for(var i = 0; i < len; i++ ) {  
        var item = results.rows.item(i)
        
        layListResults = app.CreateLayout( "linear", "Horizontal" );            
        layScroll.AddChild( layListResults );
        
        var pr1;
        if ( item.player1_amount ){
            pr1 = item.player1_amount.toFixed(1);
            glCurrResultTotals[0] = glCurrResultTotals[0] + item.player1_amount;
        } else {
            pr1 = "-";
        }
        
        txtPlayer1 = app.CreateText( pr1, widthCoef, -1 );
        if (item.player1_amount < 0 ) {
            txtPlayer1.SetTextColor("red");
        } else if ( item.player1_amount > 0 ){
            txtPlayer1.SetTextColor("green");
        }
        txtPlayer1.SetTextSize(16);
        txtPlayer1.SetBackColor("darkgray");        

        //layListResults.AddChild( txtPlayer1 );
        

        var pr2;
        if ( item.player2_amount ){
            pr2 = item.player2_amount.toFixed(1);
            glCurrResultTotals[1] = glCurrResultTotals[1] + item.player2_amount;
        } else {
            pr2 = "-";
        }
        
        txtPlayer2 = app.CreateText( pr2, widthCoef, -1 );
        if (item.player2_amount < 0 ) {
            txtPlayer2.SetTextColor("red");
        } else if ( item.player2_amount > 0 ){
            txtPlayer2.SetTextColor("green");
        }
        txtPlayer2.SetTextSize(16);
        txtPlayer2.SetBackColor("darkgray");

        var pr3;
        if ( item.player3_amount ){
            pr3 = item.player3_amount.toFixed(1);
            glCurrResultTotals[2] = glCurrResultTotals[2] + item.player3_amount;

        } else {
            pr3 = "-";
        }
        txtPlayer3 = app.CreateText( pr3, widthCoef, -1 );
        if (item.player3_amount < 0 ) {
            txtPlayer3.SetTextColor("red");
        } else if ( item.player3_amount > 0 ){
            txtPlayer3.SetTextColor("green");
        }
        txtPlayer3.SetTextSize(16);
        txtPlayer3.SetBackColor("darkgray");

        if ( names.length == 4 ) {
        
            var pr4;
            if ( item.player4_amount ){
                pr4 = item.player4_amount.toFixed(1);
                glCurrResultTotals[3] = glCurrResultTotals[3] + item.player4_amount;
    
            } else {
                pr4 = "-";
            }
            txtPlayer4 = app.CreateText( pr4, widthCoef, -1 );
            if (item.player4_amount < 0 ) {
                txtPlayer4.SetTextColor("red");
            } else if ( item.player4_amount > 0 ){
                txtPlayer4.SetTextColor("green");
            }
            txtPlayer4.SetTextSize(16);
            txtPlayer4.SetBackColor("darkgray");

            layListResults.AddChild( txtPlayer1 );
            layListResults.AddChild( txtPlayer2 );
            layListResults.AddChild( txtPlayer3 );
            layListResults.AddChild( txtPlayer4 );         
        } else {
            layListResults.AddChild( txtPlayer1 );
            layListResults.AddChild( txtPlayer2 );
            layListResults.AddChild( txtPlayer3 );
        }
        
        // test scroll step
        scrollPlays.ScrollTo( 0,layScroll.GetHeight() );
        
        //update totals
        for ( j=0; j<names.length; j++ ){
            if (glCurrResultTotals[j] < 0 ) {
                txtPlayerResults[ names[j] ].SetTextColor("red");
            } else if ( glCurrResultTotals[j] > 0 ){
                txtPlayerResults[ names[j] ].SetTextColor("green");
            } else {
                txtPlayerResults[ names[j] ].SetTextColor("white");
            }
            txtPlayerResults[ names[j] ].SetText( glCurrResultTotals[j].toFixed(1) );
        }
        
    }  
    
    scrollPlays.ScrollTo( 0,layScroll.GetHeight() );
    
}


function OnNamesOk( result ){
    dlgTxt.Hide();
    
    clearLayer();    
    
    refreshSpinGames();
    
}

function OnNamesError( error ){
    app.ShowPopup( "error inserting data: " + error );
    console.log("error inserting");
}




function btnCancelNames_OnTouch() {
    
    dlgTxt.Hide();
}


function OnTouchPlayerTitle(){

    glCurrNext = names.indexOf( this.GetText() ) - 1;
    setNextPlayer();
}


function btnNewResult_OnTouch() {

    // player on pause in 4 player gane
    var playerOnPauseIdx = -1;
    if ( names.length == 4 ){
        if( glCurrNext == 3 ){
            playerOnPauseIdx = 0;
        } else {
            playerOnPauseIdx = glCurrNext + 1;
        }
    }

    if ( !glCurrGameId || !glCurrSessionId ){
        app.ShowPopup( "either game or session is not defined" );
        return;
    }

    console.log(names);

    inputCnt = 0;
    inputIdx = -1;
    for ( j=0; j<names.length; j++ ){
        if ( inpPlayer[names[j]].GetText() ){
            inputCnt++;
            inputIdx = j;
        }
    }
    
    if ( inputCnt == 0 ){
        app.ShowPopup( "empty inputs" );   
        return;
    } else if ( inputCnt > 1 ) {
        app.ShowPopup( "to many inputs" );   
        return;
    }
    
    
    if ( inputIdx == playerOnPauseIdx ){
        app.ShowPopup( "no entry for paused player allowed" );
        return;
    }
    
    
    var p = [];
    
    var gameResult;
    
    try {
        
        for ( i=0; i<names.length; i++ ){
            console.log( names[i] + " : " + inpPlayer[ names[i] ].GetText() );
            p[names[i]] = parseFloat(inpPlayer[ names[i] ].GetText());
            
            if ( !isNaN(p[names[i]]) ){
                gameResult = p[names[i]];
            }
        }
        
        for ( i=0; i<names.length; i++ ){

            if ( i != playerOnPauseIdx ){
            
                if ( isNaN(p[names[i]]) ) {
                    p[names[i]] = -1 * gameResult;
                } else {
                    p[names[i]] = 2 * gameResult; 
                }
            } else {
                p[names[i]] = 0; 
            }
        }
        
        if ( names.length == 3 ){
            p[names[3]] = 0; 
        }
        
    } catch(error) {
        
        app.ShowPopup( "error by calculating result: " + error );   
        return;
    }

    db.ExecuteSql( "INSERT INTO plays (game_id, session, player1_amount, player2_amount, player3_amount, player4_amount)" +   
        " VALUES (?,?,?,?,?,?)", [glCurrGameId, glCurrSessionId,p[names[0]],p[names[1]],p[names[2]],p[names[3]]], 
        OnResultsOk(p), 
        OnResultsError );  

}


function OnResultsOk(p){

    //refreshResults();
    
    var s = "";  
    var tmp = "";

    console.log( "single results after insert" );

    layListResults = app.CreateLayout( "linear", "Horizontal" );            
    layScroll.AddChild( layListResults );
    
    for ( i=0; i<names.length; i++ ){
     
        var result = p[names[i]].toFixed(1);
        if (p[names[i]].toFixed(1) == 0){
            result = "-";
        }
     
        txtPlayer = app.CreateText( result, widthCoef, -1 );       
        if (p[names[i]] < 0 ) {
            txtPlayer.SetTextColor("red");
        } else if ( p[names[i]] > 0 ){
            txtPlayer.SetTextColor("green");
        }
        txtPlayer.SetTextSize(16);
        txtPlayer.SetBackColor("darkgray");        
        layListResults.AddChild( txtPlayer );
        
    }
        
    scrollPlays.ScrollTo( 0,layScroll.GetHeight() );

    for ( i=0; i<names.length; i++ ){

        glCurrResultTotals[i] = glCurrResultTotals[i] + p[names[i]];
        if (glCurrResultTotals[i] < 0 ) {
            txtPlayerResults[names[i]].SetTextColor("red");
        } else if ( glCurrResultTotals[i] > 0 ){
            txtPlayerResults[names[i]].SetTextColor("green");
        } else {
            txtPlayerResults[names[i]].SetTextColor("white");
        }
        txtPlayerResults[names[i]].SetText( glCurrResultTotals[i].toFixed(1) );
    }
    

    // total for the whole group    
    for ( i=0; i<names.length; i++ ){

        glCurrGroupResultTotals[i] = glCurrGroupResultTotals[i] + p[names[i]];
        if (glCurrGroupResultTotals[i] < 0 ) {
            txtGroupPlayerResults[names[i]].SetTextColor("red");
        } else if ( glCurrGroupResultTotals[i] > 0 ){
            txtGroupPlayerResults[names[i]].SetTextColor("green");
        } else {
            txtGroupPlayerResults[names[i]].SetTextColor("white");
        }
        txtGroupPlayerResults[names[i]].SetText( glCurrGroupResultTotals[i].toFixed(1) );
    }
    
    
    // total for the whole group points    
    // find max abs
    maxAbs = 0;
    for ( i=0; i<names.length; i++ ) 
    {
        currAbs = Math.abs( p[names[i]] );
        if ( currAbs > maxAbs ) {
            maxAbs = currAbs;
        }
    }    
    
    // increase value
    for ( i=0; i<names.length; i++ ){

        if ( p[names[i]] > 0 ){
            
            glCurrGroupPointsResultTotals[i] = glCurrGroupPointsResultTotals[i] + p[names[i]]/maxAbs;
            txtGroupPointsPlayerResults[names[i]].SetText( glCurrGroupPointsResultTotals[i].toFixed(1) );
            
        }
    }
    
    // adjust colors
    for ( i=0; i<names.length; i++ ){
        txtGroupPointsPlayerResults[names[i]].SetTextColor(getColor(glCurrGroupPointsResultTotals, glCurrGroupPointsResultTotals[i]));
    }    

    // increase value
    for ( i=0; i<names.length; i++ ){

        if ( p[names[i]] > 0 && p[names[i]]/maxAbs == 1){
            
            glCurrGroupCntAttackResultTotals[i] = glCurrGroupCntAttackResultTotals[i] + 1;
            txtGroupCntAttackPlayerResults[names[i]].SetText( glCurrGroupCntAttackResultTotals[i].toFixed(0) );
        }
    }
    
    // adjust colors
    for ( i=0; i<names.length; i++ ){
        txtGroupCntAttackPlayerResults[names[i]].SetTextColor(getColor( glCurrGroupCntAttackResultTotals, glCurrGroupCntAttackResultTotals[i] ));
    }

    // increase values
    for ( i=0; i<names.length; i++ ){

        if ( p[names[i]] > 0 && p[names[i]]/maxAbs == 0.5){
            
            glCurrGroupCntDefenseResultTotals[i] = glCurrGroupCntDefenseResultTotals[i] + 1;
            txtGroupCntDefensePlayerResults[names[i]].SetText( glCurrGroupCntDefenseResultTotals[i].toFixed(0) );
        }
    }
    
    // adjust colors
    for ( i=0; i<names.length; i++ ){
        txtGroupCntDefensePlayerResults[names[i]].SetTextColor(getColor(glCurrGroupCntDefenseResultTotals, glCurrGroupCntDefenseResultTotals[i] ));
    }

    // clear edit fields
    for ( i=0; i<names.length; i++ ){
        inpPlayer[names[i]].SetText("");    
    }
    
    setNextPlayer();        
    
}


function OnResultsError(error){

    app.ShowPopup("error by inserting result into db: "+ error);
    
    scrollPlays.ScrollTo( 1,1 );
    
}


function spinGames_OnChange( item ){
 
    console.log( "Item = " + item );      
    
    idsToDelete = spinGames.GetText().split(":");
    glCurrGameId = parseInt(idsToDelete[0]);
    
    clearLayer();    
    
    
    setNames(item);    
    
    refreshSessions();
    
}


function spinSessions_OnChange( item ){
 
    console.log( "Item = " + item );       
    
    glCurrSessionId = parseInt(spinSessions.GetText());
    
    refreshResults();
    
}


function setNames(item){
    
    if ( item ) {
        
        // create fields with player nems
        layPNames = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layPNames );

        twoParts = item.split( ":" );
        names = [];
        names = twoParts[1].split( "-" );
        
        widthCoef = 0.3;
        if ( names.length == 4 ) {
            widthCoef = 0.225;
        }
        txtPlayerTitle = [];
        for ( i=0; i < names.length; i++ ){
            
            console.log( "jmeno " + i + " " + names[i] );
            txtPlayerTitle[ names[i] ] = app.CreateText( "", widthCoef, -1 );                    
            txtPlayerTitle[ names[i] ].SetTextSize(24);
            txtPlayerTitle[ names[i] ].SetTextColor("white");
            txtPlayerTitle[ names[i] ].SetTextShadow( 0.6, 0.02, 0.02 );
            txtPlayerTitle[ names[i] ].SetBackColor("darkgray");
            txtPlayerTitle[ names[i] ].SetMargins( 0, 0.01, 0, 0.01 );
            
            txtPlayerTitle[ names[i] ].SetOnLongTouch( OnTouchPlayerTitle );
            
            layPNames.AddChild( txtPlayerTitle[ names[i] ] );                
        }
        
        setNextPlayer( glCurrNext );        
    
        // create input fields
        layPResults = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layPResults );

        inpPlayer = [];

        for ( i=0; i < names.length; i++ ){
            inpPlayer[ names[i] ] = app.CreateTextEdit( "", widthCoef, -1, "Number" );
            layPResults.AddChild( inpPlayer[ names[i] ] );    
        }

        // report result
        btnNewResult = app.CreateButton( "store new result",  0.9, -1 );
        btnNewResult.SetOnTouch( btnNewResult_OnTouch );
        layMain.AddChild( btnNewResult );        
    
        // create play list
        scrollPlays = app.CreateScroller( 0.9, 0.21 );
        layMain.AddChild( scrollPlays );    
        
        layScroll = app.CreateLayout( "Linear", "Vertical,FillX" );
        scrollPlays.AddChild( layScroll );
        
        scrollPlays.ScrollTo( 0,layScroll.GetHeight() );
        
        // create fields with player totals
        layResults = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layResults );
        
        txtPlayerResults = [];    
        
        for ( i=0; i < names.length; i++ ){
            txtPlayerResults[ names[i] ] = app.CreateText( "", widthCoef, -1 );                    
            txtPlayerResults[ names[i] ].SetTextSize(24);
            txtPlayerResults[ names[i] ].SetBackColor("darkgray");
            txtPlayerResults[ names[i] ].SetMargins( 0, 0.01, 0, 0.01 );
            layResults.AddChild( txtPlayerResults[ names[i] ] );   
 
            txtPlayerTitle[ names[i] ].SetText(names[i]);
            
        }

        // group totals     
        // create fields with player totals
        layGroupResults = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layGroupResults );
        
        txtGroupPlayerResults = [];    
        for ( i=0; i < names.length; i++ ){
            txtGroupPlayerResults[ names[i] ] = app.CreateText( "", widthCoef, -1 );                    
            txtGroupPlayerResults[ names[i] ].SetTextSize(20);
            txtGroupPlayerResults[ names[i] ].SetBackColor("black");
            txtGroupPlayerResults[ names[i] ].SetMargins( 0, 0.01, 0, 0.01 );
            layGroupResults.AddChild( txtGroupPlayerResults[ names[i] ] );   
        }
            
        // group points totals     
        // create fields with player totals
        layGroupPointsResults = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layGroupPointsResults );
        
        txtGroupPointsPlayerResults = [];    
        for ( i=0; i < names.length; i++ ){
            txtGroupPointsPlayerResults[ names[i] ] = app.CreateText( "", widthCoef, -1 );                    
            txtGroupPointsPlayerResults[ names[i] ].SetTextSize(20);
            txtGroupPointsPlayerResults[ names[i] ].SetBackColor("black");
            txtGroupPointsPlayerResults[ names[i] ].SetMargins( 0, 0.01, 0, 0.01 );
            layGroupPointsResults.AddChild( txtGroupPointsPlayerResults[ names[i] ] );   
        }        
    
        
        // group counts attack totals     
        // create fields with player totals
        layGroupCntAttackResults = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layGroupCntAttackResults );
        
        txtGroupCntAttackPlayerResults = [];    
        for ( i=0; i < names.length; i++ ){
            txtGroupCntAttackPlayerResults[ names[i] ] = app.CreateText( "", widthCoef, -1 );                    
            txtGroupCntAttackPlayerResults[ names[i] ].SetTextSize(20);
            txtGroupCntAttackPlayerResults[ names[i] ].SetBackColor("black");
            txtGroupCntAttackPlayerResults[ names[i] ].SetMargins( 0, 0.01, 0, 0.01 );
            layGroupCntAttackResults.AddChild( txtGroupCntAttackPlayerResults[ names[i] ] );   
        }

        // group counts defense totals     
        // create fields with player totals
        layGroupCntDefenseResults = app.CreateLayout( "linear", "Horizontal" );    
        layMain.AddChild( layGroupCntDefenseResults );
        
        txtGroupCntDefensePlayerResults = [];    
        for ( i=0; i < names.length; i++ ){
            txtGroupCntDefensePlayerResults[ names[i] ] = app.CreateText( "", widthCoef, -1 );                    
            txtGroupCntDefensePlayerResults[ names[i] ].SetTextSize(20);
            txtGroupCntDefensePlayerResults[ names[i] ].SetBackColor("black");
            txtGroupCntDefensePlayerResults[ names[i] ].SetMargins( 0, 0.01, 0, 0.01 );
            layGroupCntDefenseResults.AddChild( txtGroupCntDefensePlayerResults[ names[i] ] );   
        }
        
        txtFooter = app.CreateText( "", 0.9, -1, "Multiline" );
        txtFooter.SetTextSize(12);
        //txtFooter.SetText("mariasky.cz 2013-2019");
        txtFooter.SetText("total amounts / group; total points / group (1xA, 0,5xD);\ntotal counts of winning attacks / group;\ntotal counts of winning defenses / group");
        txtFooter.SetBackColor("black");
        txtFooter.SetMargins( 0, 0.01, 0, 0.01 );
        layMain.AddChild( txtFooter );
        
        inpPlayer[ names[0] ].Focus();
        
        console.log( "try to refresh stats..." );
        refreshStats();
        
    }
}    
					  			